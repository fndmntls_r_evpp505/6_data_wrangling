# Data Wrangling

This repo contains the lessons learning how to wrangle data using R!

Refer to the chapters on wrangling in R for Data Science:

- [Wrangle](https://r4ds.had.co.nz/wrangle-intro.html)

More specific topics

- [Tidy Data](https://r4ds.had.co.nz/tidy-data.html)
- [Pipes](https://r4ds.had.co.nz/pipes.html)

You can also look at the [cheatsheet](https://github.com/rstudio/cheatsheets/raw/master/data-transformation.pdf) for the `dplyr` package we will be using in class.  There is a copy of the cheatsheet in the repo, but always check the site for the latest version; these packages change often.